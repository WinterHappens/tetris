﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CA_TetrisGame
{
    class GameManager
    {
        public static Random rnd = new Random();

        public void Run()
        {
            bool merger = true;

            Field field = new Field(10, 20, 5, 5);

            int number = rnd.Next(1, 8);
            int x = field.X + field.Width / 2 - 1;
            int y = field.Y + 1;

            Figure figure = NextFigure(number, x, y);
            field.FillField(merger, figure);

            while (!(Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Escape))
            {
                Console.CursorVisible = false;
                Console.Clear();

                if (merger == false)
                {
                    field.FillField(merger, figure);
                    
                    merger = true;

                    number = rnd.Next(1, 8);
                    figure = NextFigure(number, x, y);
                }

                figure.DrawFigure();
                //field.FillField(merger, figure);
                field.DrawField();

                figure.DrawFigure();

                merger = figure.Move(field);

                while (Console.KeyAvailable)
                {
                    switch (Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.RightArrow:
                            figure.MoveRight(field);
                            break;
                        case ConsoleKey.LeftArrow:
                            figure.MoveLeft(field);
                            break;
                        case ConsoleKey.Spacebar:
                            figure.RotateFigure(field);
                            break;
                    }
                }

                Thread.Sleep(1000);
            }
        }

        private Figure NextFigure(int number, int x, int y)
        {
            switch (number)
            {
                case 1:
                    return new Figure1(x, y);
                    break;
                case 2:
                    return new Figure2(x, y);
                    break;
                case 3:
                    return new Figure3(x, y);
                    break;
                case 4:
                    return new Figure4(x, y);
                    break;
                case 5:
                    return new Figure5(x, y);
                    break;
                case 6:
                    return new Figure6(x, y);
                    break;
                case 7:
                    return new Figure7(x, y);
                    break;
            }
            return null;
        }
    }
}
