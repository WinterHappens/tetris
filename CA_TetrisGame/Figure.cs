﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_TetrisGame
{
    class Figure
    {
        private int x, y;
        protected char[,] cells;

        public Figure(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public bool Move(Field field)
        {
            while (y != field.Y + field.Height - cells.GetLength(1) - 1)
            {
                y++;

                for (int i = x + 1; i < cells.GetLength(0) + x; i++)
                {
                    for (int j = y + 1; j < cells.GetLength(1) + y; j++)
                    {
                        if (cells[i - x, j - y - 1] == 'X' && field.FieldCells[i - field.X, j - field.Y] == 'X')
                        {
                            return false;
                        }
                    }
                }

                return true;
            }

            return false;
        }

        public void RotateFigure(Field field)
        {
            int maxLength;

            if (cells.GetLength(1) >= cells.GetLength(0))
            {
                maxLength = cells.GetLength(1);
            }
            else
            {
                maxLength = cells.GetLength(0);
            }

            if (y <= field.Y + field.Height - maxLength)
            {
                char[,] resultFigure = new char[cells.GetLength(1), cells.GetLength(0)];

                for (int i = 0; i < resultFigure.GetLength(0); i++)
                {
                    for (int j = 0; j < resultFigure.GetLength(1); j++)
                    {
                        resultFigure[i, j] = cells[resultFigure.GetLength(1) - j - 1, i];
                    }
                }

                cells = resultFigure;
            }
        }

        public void DrawFigure()
        {
            for (int i = 0; i < cells.GetLength(0); i++)
            {
                for (int j = 0; j < cells.GetLength(1); j++)
                {
                    Console.SetCursorPosition(x + i, y + j);
                    Console.Write(cells[i, j]);
                }
            }
        }

        public void MoveLeft(Field field)
        {
            if (x > field.X + 1)
            {
                x--;
            }
        }

        public void MoveRight(Field field)
        {
            if (x < field.X + field.Width - cells.GetLength(1))
            {
                x++;
            }
        }

        public int X
        {
            get { return x; }
        }

        public int Y
        {
            get { return y; }
        }

        public int Height
        {
            get { return cells.GetLength(1); }
        }

        public int Width
        {
            get { return cells.GetLength(0); }
        }

        public char[,] Cells
        {
            get { return cells; }
        }
    }
}
