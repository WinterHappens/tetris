﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_TetrisGame
{
    class Figure6 : Figure
    {
        public Figure6(int x, int y) : base(x, y)
        {
            cells = new char[,]
            {
                {'X', 'X',' '},
                {' ', 'X','X'}
            };
        }
    }
}
