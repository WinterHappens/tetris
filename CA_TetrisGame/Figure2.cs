﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CA_TetrisGame
{
    class Figure2 : Figure
    {
        public Figure2(int x, int y) : base(x, y)
        {
            cells = new char[,]
            {
                {'X','X','X','X'}
            };
        }
    }
}
