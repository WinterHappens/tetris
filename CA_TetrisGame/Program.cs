﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CA_TetrisGame
{
    class Program
    {
        static void Main(string[] args)
        {
            GameManager manager = new GameManager();

            manager.Run();
        }
    }
}
