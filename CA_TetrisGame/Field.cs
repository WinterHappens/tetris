﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_TetrisGame
{
    class Field
    {
        private int width;
        private int height;
        private int x1, y1;

        protected char[,] field;

        public Field(int width, int height, int x1, int y1)
        {
            this.height = height + 2;
            this.width = width + 2;
            this.x1 = x1;
            this.y1 = y1;

            field = new char[width + 2, height + 2];
        }

        public void FillField(bool merger, Figure figure)
        {
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    if (i == 0)
                    {
                        field[i, j] = '*';
                    }

                    if (j == 0)
                    {
                        field[i, j] = '*';
                    }

                    if (i == field.GetLength(0) - 1)
                    {
                        field[i, j] = '*';
                    }

                    if (j == field.GetLength(1) - 1)
                    {
                        field[i, j] = '*';
                    }
                }
            }


            if (merger == false)
            {
                for (int i = figure.X; i < figure.X + figure.Width; i++)
                {
                    for (int j = figure.Y; j < figure.Y + figure.Height; j++)
                    {
                        if (figure.Cells[i - figure.X, j - figure.Y] == 'X')
                        {
                            field[i - x1, j - y1] = 'X';
                        }
                    }
                }
            }
        }

        public void DrawField()
        {
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    Console.SetCursorPosition(x1 + i, y1 + j);
                    Console.Write(field[i, j]);
                }
                Console.WriteLine();
            }
        }

        public int X
        {
            get { return x1; }
        }

        public int Y
        {
            get { return y1; }
        }

        public int Height
        {
            get { return height; }
        }

        public int Width
        {
            get { return width; }
        }

        public char[,] FieldCells
        {
            get { return field; }
        }
    }
}
