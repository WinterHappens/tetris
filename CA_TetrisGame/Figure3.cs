﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_TetrisGame
{
    class Figure3 : Figure
    {
        public Figure3(int x, int y) : base(x, y)
        {
            cells = new char[,]
            {
                {'X',' '},
                {'X',' '},
                {'X','X'}
            };
        }
    }
}
